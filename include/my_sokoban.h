/*
** EPITECH PROJECT, 2017
** My Sokoban
** File description:
** Header for my_sokoban
*/

#ifndef MY_SOKOBAN_H_
#define MY_SOKOBAN_H_

/* player structure*/
typedef struct player
{
	int	x;
	int	y;
}player_t;

/* check_map.c */
int     check_win(char**, char**);
int     check_mouv_box(char**, int, int);
int     check_map_end(char**, char**);

/* copy_tab.c */
char    **copy_tab_char(char**);


/* display_help.c */
void	display_help(void);

/* game_function.c */
char    **move_player(char**, player_t*);
int     check_size(char**);
player_t take_player_pos(char**);
char    **check_storage(char**, char**);
int     display_game(char**, char**, int);

/* get_data.c*/
int     get_nb_column(int);
int     get_nb_line(int, int);
char    *get_first_line(int, int);
char    *get_line(int, int);
char    *get_last_line(int, int);

/* memory_tab.c */
void	free_tab_char(char**);

/* move_check.c */
int     check_up(char**, player_t*);
int     check_right(char**, player_t*);
int     check_down(char**, player_t*);
int     check_left(char**, player_t*);

/* move_function.c*/

char	**player_up(char**, player_t*);
char	**player_right(char**, player_t*);
char	**player_down(char**, player_t*);
char	**player_left(char**, player_t*);

/* my_sokoban.c*/
void    check_if_valid(char**);
char    **get_map(char*);
int     my_sokoban(char*);

#endif /* MY_SOKOBAN_H_ */
