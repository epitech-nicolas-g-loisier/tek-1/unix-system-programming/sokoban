##
## EPITECH PROJECT, 2017
## My ls
## File description:
## Makefile
##

SRC_DIR	=	$(realpath src)

CFLAGS	=	-Wall -Wextra -Iinclude/ -L lib/ -lmy

SRC	=	$(SRC_DIR)/my_sokoban.c	\
		$(SRC_DIR)/display_help.c	\
		$(SRC_DIR)/get_data.c	\
		$(SRC_DIR)/memory_tab.c	\
		$(SRC_DIR)/game_function.c	\
		$(SRC_DIR)/move_function.c	\
		$(SRC_DIR)/move_check.c	\
		$(SRC_DIR)/check_map.c	\
		$(SRC_DIR)/copy_tab.c

OBJ	=	$(SRC:.c=.o)

NAME	=	my_sokoban

all:            $(NAME)

$(NAME):	$(OBJ)
		make -C lib/my/
		gcc -o $(NAME) $(OBJ) $(CFLAGS) -lncurses

clean:
		rm -f $(OBJ)
		make clean -C lib/my/

fclean:		clean
		make fclean -C lib/my/
		rm -f $(NAME)

re:		fclean all

test_run:
		make -C tests/
		./tests/unit-tests
		make fclean -C tests/
