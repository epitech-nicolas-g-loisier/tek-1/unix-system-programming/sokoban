/*
** EPITECH PROJECT, 2017
** lib
** File description:
** allocate memory for char*
*/

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include "my.h"

char	*my_strdup(char const *str)
{
	int	count = 0;
	char	*new_str = malloc(sizeof(char) * (my_strlen(str) + 1));

	while (str[count] != 0){
		new_str[count] = str[count];
		count++;
	}
	new_str[count] = 0;
	return (new_str);
}
