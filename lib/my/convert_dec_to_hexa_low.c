/*
** EPITECH PROJECT, 2017
** Lib: convertisseur
** File description:
** convert decimal to hexadecimal with "abcdef"
*/

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "my.h"

char	find_hexa_low(long power, int nb)
{
	long	tmp = 0;
	long	div = my_compute_power_it(16, power);
	char	res;
	char	*chara = "0123456789abcdef";

	tmp = nb / div;
	res = chara[tmp];
	return (res);
}

char	*convert_to_exa_low(int nb)
{
	long	power = 0;
	long	tmp_nb = nb;
	char	*result = malloc(sizeof(char) * 100);
	int	i = 0;

	while (nb != 0){
		power = 0;
		tmp_nb = nb;
		while (tmp_nb >= 16){
			tmp_nb = tmp_nb / 16;
			power++;
		} if (power == 0){
			result[0] = find_hexa_low(0, nb);
			nb = 0;
		} else {
			result[power] = find_hexa_low(power, nb);
			nb = nb % my_compute_power_it(16, power);
		}
		i++;
	}
	return (my_revstr(result));
}
