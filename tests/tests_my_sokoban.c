/*
** EPITECH PROJECT, 2017
** My Sokoban
** File description:
** Unit tests
*/

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <criterion/criterion.h>
#include "my_sokoban.h"
#include "my.h"

Test(my_sokoban, take_player_position)
{
	player_t  player;
	char	*map[7];

	map[0] = "#####\n";
	map[1] = "# P #\n";
	map[2] = "# O #\n";
	map[3] = "# X #\n";
	map[4] = "#   #\n";
	map[5] = "#####\n";
	map[6] = NULL;
	player = take_player_pos(map);
	cr_expect_eq(player.x, 1);
	cr_expect_eq(player.y, 2);
}

Test(my_sokoban, player_up)
{
	player_t player;
	char	**map = malloc(sizeof(char*) * 7);

	map[0] = my_strdup("#####\n");
	map[1] = my_strdup("#   #\n");
	map[2] = my_strdup("# OP#\n");
	map[3] = my_strdup("# X #\n");
	map[4] = my_strdup("#   #\n");
	map[5] = my_strdup("#####\n");
	map[6] = NULL;
	player = take_player_pos(map);
	map = player_up(map, &player);
	cr_expect_eq(map[1][3], 'P');
	free_tab_char(map);
}

Test(my_sokoban, player_right)
{
	player_t player;
	char	**map = malloc(sizeof(char*) * 7);

	map[0] = my_strdup("#####\n");
	map[1] = my_strdup("# P #\n");
	map[2] = my_strdup("# O #\n");
	map[3] = my_strdup("# X #\n");
	map[4] = my_strdup("#   #\n");
	map[5] = my_strdup("#####\n");
	map[6] = NULL;
	player = take_player_pos(map);
	map = player_right(map, &player);
	cr_expect_eq(map[1][3], 'P');
	free_tab_char(map);
}

Test(my_sokoban, player_down)
{
	player_t player;
	char	**map = malloc(sizeof(char*) * 7);

	map[0] = my_strdup("#####\n");
	map[1] = my_strdup("# P #\n");
	map[2] = my_strdup("# O #\n");
	map[3] = my_strdup("# X #\n");
	map[4] = my_strdup("#   #\n");
	map[5] = my_strdup("#####\n");
	map[6] = NULL;
	player = take_player_pos(map);
	map = player_down(map, &player);
	cr_expect_eq(map[2][2], 'P');
	free_tab_char(map);
}

Test(my_sokoban, player_left)
{
	player_t player;
	char	**map = malloc(sizeof(char*) * 7);

	map[0] = my_strdup("#####\n");
	map[1] = my_strdup("# P #\n");
	map[2] = my_strdup("# O #\n");
	map[3] = my_strdup("# X #\n");
	map[4] = my_strdup("#   #\n");
	map[5] = my_strdup("#####\n");
	map[6] = NULL;
	player = take_player_pos(map);
	map = player_left(map, &player);
	cr_expect_eq(map[1][1], 'P');
	free_tab_char(map);
}
