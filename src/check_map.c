/*
** EPITECH PROJECT, 2017
** My Sokoban
** File description:
** check the end of the game
*/

#include <curses.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include "my.h"
#include "my_sokoban.h"

int	check_win(char **map, char **map_save)
{
	int	x = 0;
	int	y = 0;

	while (map[x] != NULL){
		y = 0;
		while (map[x][y] != '\n'){
			if (map_save[x][y] == 'O' && map[x][y] != 'X'){
				return -1;
			}
			y++;
		}
		x++;
	}
	return 0;
}

int	check_mouv_box(char **map, int x, int y)
{
	if (map[x + 1][y] != '#' && map[x - 1][y] != '#' &&
		map[x + 1][y] != 'X' && map[x - 1][y] != 'X'){
		return 0;
	}
	else if (map[x][y + 1] != '#' && map[x][y - 1] != '#'
		&& map[x][y + 1] != 'X' && map[x][y - 1] != 'X'){
		return 0;
	} else {
		return 1;
	}
}

int	check_lose(char **map)
{
	int	x = 0;
	int	y = 0;
	int	check = 0;
	int	block = 0;

	while (map[x] != NULL){
		y = 0;
		while (map[x][y] != '\n'){
			if (map[x][y] == 'X'){
				block += check_mouv_box(map, x, y);
				check++;
			}
			y++;
		}
		x++;
	}
	if (check == block)
		return 1;
	else
		return -1;
}

int	check_map_end(char **map, char **map_save)
{
	if (check_win(map, map_save) == 0)
		return 0;
	else if (check_lose(map) == 1)
		return 1;
	return -1;
}
