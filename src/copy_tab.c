/*
** EPITECH PROJECT, 2017
** My Sokoban
** File description:
** copy tab
*/

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include "my.h"
#include "my_sokoban.h"

char	**copy_tab_char(char **map)
{
	int	x = 0;
	int	y = 0;
	char	**tab;

	while (map[x] != NULL)
		x++;
	tab = malloc(sizeof(char*) * (x + 1));
	x = 0;
	while (map[x] != NULL){
		y = 0;
		tab[x] = malloc(sizeof(char) * (my_strlen(map[x]) + 1));
		while (map[x][y] != 0){
			tab[x][y] = map[x][y];
			y++;
		}
		x++;
	}
	tab[x] = NULL;
	return tab;
}
