/*
** EPITECH PROJECT, 2017
** My Sokoban
** File description:
** copy of the sokoban's game in terminal mode
*/

#include <curses.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include "my.h"
#include "my_sokoban.h"

void	check_if_valid(char **map)
{
	int	box = 0;
	int	storage = 0;
	int	x = 0;
	int	y = 0;

	while (map[x] != NULL){
		y = 0;
		while (map[x][y] != '\n'){
			if (map[x][y] == 'X')
				box++;
			else if (map[x][y] == 'O')
				storage++;
			y++;
		}
		x++;
	}
	if (storage == 0)
		exit(0);
	else if (storage != box)
		exit(84);
}

void	check_if_one_player(char **map)
{
	int	x = 1;
	int	y = 1;
	int	player = 0;

	while (map[x] != NULL){
		y = 1;
		while (map[x][y] != '\n'){
			if (map[x][y] == 'P')
				player++;
			y++;
		}
		x++;
	}
	if (player != 1)
		exit(84);
}

char	**get_map(char *filepath)
{
	int	fd = open(filepath, O_RDONLY);
	int	count = 0;
	int	column = get_nb_column(fd);
	int	line = get_nb_line(fd, column);
	char	**map = malloc(sizeof(char*) * (line + 1));

	close(fd);
	fd = open(filepath, O_RDONLY);
	map[count] = get_first_line(fd, column);
	count++;
	while (count < line - 1){
		map[count] = get_line(fd, column);
		count++;
	}
	map[count] = get_last_line(fd, column);
	close(fd);
	map[count + 1] = NULL;
	check_if_valid(map);
	check_if_one_player(map);
	return map;
}

int	my_sokoban(char *filepath)
{
	int	end = -1;
	char	**map = get_map(filepath);
	char	**game = copy_tab_char(map);

	initscr();
	while (end == -1 || end == 2){
		end = -1;
		map = copy_tab_char(game);
		noecho();
		curs_set(0);
		end = display_game(map, game, end);
	}
	endwin();
	free_tab_char(map);
	free_tab_char(game);
	return end;
}

int	main(int ac, char **av)
{
	int	end = 0;

	if (ac != 2){
		write(2, "try with: ./my_sokoban -h\n", 26);
		return 84;
	} else if (my_strcmp("-h", av[1]) == 0)
		display_help();
	else {
		if (open(av[1], O_RDONLY) == -1){
			write(2, "invalid file\n", 13);
			return 84;
		}
		else
			end = my_sokoban(av[1]);
	}
	return end;
}
