/*
** EPITECH PROJECT, 2017
** My Sokoban
** File description:
** display help
*/

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include "my.h"
#include "my_sokoban.h"

void	display_help(void)
{
	write(1, "USAGE\n", 6);
	write(1, "\t./my_sokoban map\n", 18);
	write(1, "DESCRIPTION\n", 12);
	write(1, "\tmap file representing the warehouse map, containing ", 53);
	write(1, "'#' for walls,\n\t\t‘P’ for the player, ‘X’ for boxes", 50);
	write(1, " and ‘O’ for storage locations.\n", 36);
}
