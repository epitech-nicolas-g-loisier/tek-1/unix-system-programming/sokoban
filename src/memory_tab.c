/*
** EPITECH PROJECT, 2017
** lib
** File description:
** free or malloc tab
*/

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

void	free_tab_char(char **tab)
{
	int	i = 0;

	while (tab[i] != NULL){
		free(tab[i]);
		i++;
	}
	free(tab);
}
