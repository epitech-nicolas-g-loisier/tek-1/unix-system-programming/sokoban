/*
** EPITECH PROJECT, 2017
** My Sokoban
** File description:
** check if player can move
*/

#include <curses.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include "my.h"
#include "my_sokoban.h"

int	check_up(char **map, player_t *player)
{
	char	pos_up = map[player->x - 1][player->y];
	char	pos_box = map[player->x - 2][player->y];

	if (pos_up != '#' && pos_up != 'X')
		return 0;
	else if (pos_up == 'X' && (pos_box == 32 || pos_box == 'O'))
		return 1;
	else
		return -1;
}

int	check_right(char **map, player_t *player)
{
	char	pos_right = map[player->x][player->y + 1];
	char	pos_box = map[player->x][player->y + 2];

	if (pos_right != '#' && pos_right != 'X')
		return 0;
	else if (pos_right == 'X' && (pos_box == 32 || pos_box == 'O'))
		return 1;
	else
		return -1;
}

int	check_down(char **map, player_t *player)
{
	char	pos_down = map[player->x + 1][player->y];
	char	pos_box = map[player->x + 2][player->y];

	if (pos_down != '#' && pos_down != 'X')
		return 0;
	else if (pos_down == 'X' && (pos_box == 32 || pos_box == 'O'))
		return 1;
	else
		return -1;
}

int	check_left(char **map, player_t *player)
{
	char	pos_left = map[player->x][player->y - 1];
	char	pos_box = map[player->x][player->y - 2];

	if (pos_left != '#' && pos_left != 'X')
		return 0;
	else if (pos_left == 'X' && (pos_box == 32 || pos_box == 'O'))
		return 1;
	else
		return -1;
}
