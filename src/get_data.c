/*
** EPITECH PROJECT, 2017
** my sokoban
** File description:
** get data from file
*/

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include "my.h"
#include "my_sokoban.h"

int	get_nb_column(int fd)
{
	char	*buffer = malloc(sizeof(char) * 2);
	int	count	= 0;
	int	size = 0;

	buffer[0] = 0;
	while (buffer[0] != '\n'){
		size = read(fd, buffer, 1);
		if (size <= 0){
			write(2, "The map is invalid\n", 19);
			exit(84);
		}
		count++;
	}
	free(buffer);
	return count;
}

int	get_nb_line(int fd, int column)
{
	char	*buffer = malloc(sizeof(char) * (column + 1));
	int	count	= 0;
	int	size = column;

	while (size > 0){
		size = read(fd, buffer, column);
		count++;
	}
	free(buffer);
	return count;
}
char	*get_first_line(int fd, int size)
{
	char	*line = malloc(sizeof(char) * size + 1);
	int	i = 0;

	read(fd, line, size);
	while (i < size){
		if (line[i] != '#' && line[i] != '\n'){
			write(2, "The map is invalid\n", 19);
			exit(84);
		}
		i++;
	}
	line[size] = 0;
	return line;
}

char	*get_line(int fd, int size)
{
	char	*line = malloc(sizeof(char) * size + 1);
	int	i = 0;
	int	check = 0;

	read(fd, line, size);
	line[size] = 0;
	if (line[0] != '#' || line[size - 2] != '#'){
		write(2, "The map is invalid\n", 19);
		exit(84);
	}
	while (line[i] != '\n' && line[i] != 0){
		check = 0;
		if (line[i] != '#' && line[i] != 'O' && line[i] != 'P')
			check = 1;
		else if (line[i] != 'X' && line[i] != 32)
			check += 1;
		if (check == 2){
			write(2, "The map is invalid\n", 19);
			exit(84);
		}
		i++;
	}
	return line;
}

char	*get_last_line(int fd, int size)
{
	char	*line = malloc(sizeof(char) * size + 1);
	int	i = 0;

	read(fd, line, size);
	while (i < size){
		if (line[i] != '#' && line[i] != '\n'){
			write(2, "The map is invalid\n", 19);
			exit(84);
		}
		i++;
	}
	line[size] = 0;
	return line;
}
