/*
** EPITECH PROJECT, 2017
** My Sokoban
** File description:
** function to play
*/

#include <curses.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include "my.h"
#include "my_sokoban.h"

char	**move_player(char **map, player_t *player)
{
	int	i = 0;
	char	*buffer = malloc(sizeof(char) * 4);
	char	*check = "ACBD";
	char	**(*move_function[5])(char**, player_t*);

	move_function[0] = player_up;
	move_function[1] = player_right;
	move_function[2] = player_down;
	move_function[3] = player_left;
	clear();
	read(0, buffer, 3);
	while (check[i] != buffer[2] && check[i] != 0){
		i++;
	}
	if (i < 4){
		map = move_function[i](map, player);
	}
	free(buffer);
	return map;
}

int	check_size(char **map)
{
	int	x = 0;
	int	y = 0;
	int	i = 0;

	clear();
	x = COLS / 2 - 18 / 2;
	y = LINES / 2 - 1;
	while (map[i] != NULL)
		i++;
	if (COLS < my_strlen(map[0])){
		mvaddstr(y, x, "Terminal to small!");
		mvaddstr(y + 1, x - 4, "Please enlarge the terminal");
		return 1;
	} else if (LINES < i){
		mvaddstr(y, x, "Terminal to small!");
		mvaddstr(y + 1, x - 4, "Please enlarge the terminal");
		return 1;
	} else {
		return 0;
	}
}

player_t take_player_pos(char **map)
{
	player_t player;
	int	x = 0;
	int	y = 0;

	while (map[x][y] != 'P'){
		x++;
		y = 0;
		while (map[x][y] != 0 && map[x][y] != 'P'){
			y++;
		}
	}
	player.x = x;
	player.y = y;
	return player;
}

char	**check_storage(char **map, char **map_save)
{
	int	x = 0;
	int	y = 0;

	while (map_save[x] != NULL){
		y = 0;
		while (map_save[x][y] != '\n'){
			if (map_save[x][y] == 'O' && map[x][y] == 32)
				map[x][y] = 'O';
			y++;
		}
		x++;
	}
	return map;
}

int	display_game(char **map, char **map_save, int end)
{
	int	i = 0;
	player_t player = take_player_pos(map);

	timeout(0);
	int o = 0;
	while (end == -1 ){
		o++;
		refresh();
		if (check_size(map) == 0){
			while (map[i] != NULL){
				mvaddstr(i, 0, map[i]);
				i++;
			}
			i = 0;
			getch();
			map = move_player(map, &player);
			map = check_storage(map, map_save);
			end = check_map_end(map, map_save);
		}
		printw("test: %d", o);
	}
	return end;
}
