/*
** EPITECH PROJECT, 2017
** My Sokoban
** File description:
** function to move the player
*/

#include <curses.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include "my.h"
#include "my_sokoban.h"

char	**player_up(char **map, player_t *player)
{
	if (map[player->x - 1][player->y] != '#'){
		if (check_up(map, player) == 0){
			map[player->x][player->y] = 32;
			map[player->x - 1][player->y] = 'P';
			player->x--;
		}
		else if (check_up(map, player) == 1){
			map[player->x][player->y] = 32;
			map[player->x - 1][player->y] = 'P';
			map[player->x - 2][player->y] = 'X';
			player->x--;
		}
	}
	return map;
}

char	**player_right(char **map, player_t *player)
{
	if (check_right(map, player) == 0){
		map[player->x][player->y] = 32;
		map[player->x][player->y + 1] = 'P';
		player->y++;
	}
	else if (check_right(map, player) == 1){
		map[player->x][player->y] = 32;
		map[player->x][player->y + 1] = 'P';
		map[player->x][player->y + 2] = 'X';
		player->y++;
	}
	return map;
}

char	**player_down(char **map, player_t *player)
{
	if (map[player->x + 1][player->y] != '#'){
		if (check_down(map, player) == 0){
			map[player->x][player->y] = 32;
			map[player->x + 1][player->y] = 'P';
			player->x++;
		}
		else if (check_down(map, player) == 1){
			map[player->x][player->y] = 32;
			map[player->x + 1][player->y] = 'P';
			map[player->x + 2][player->y] = 'X';
			player->x++;
		}
	}
	return map;
}

char	**player_left(char **map, player_t *player)
{
	if (check_left(map, player) == 0){
		map[player->x][player->y] = 32;
		map[player->x][player->y - 1] = 'P';
		player->y--;
	}
	else if (check_left(map, player) == 1){
		map[player->x][player->y] = 32;
		map[player->x][player->y - 1] = 'P';
		map[player->x][player->y - 2] = 'X';
		player->y--;
	}
	return map;
}
